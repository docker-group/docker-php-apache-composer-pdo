FROM php:7.2-apache

RUN ssh-keyscan gitlab.com > ~/.ssh/known_hosts; ssh-keyscan github.com >> ~/.ssh/known_hosts; mkdir -p /var/www/sms-service
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && php composer-setup.php --install-dir=/usr/local/bin --filename=composer
RUN a2enmod rewrite
RUN apt-get update && \
    apt-get install -y git unzip zlib1g-dev && \
    echo "Clean apt junk" && \
    rm -rf /var/lib/apt/lists/*
RUN docker-php-ext-install pdo pdo_mysql zip 
WORKDIR /var/www/